**Okapi Olifant** is a cross-platform application to manage translation memories. It allows you to create, modify, import, export and use translation memories and work with files such as TMX, PO, XLIFF, and other translation formats.

The snapshots (development versions) of Olifant can be downloaded from http://okapiframework.org/snapshots/

Please note this project is still ALPHA and to use for test only. For production you can still use the older version of [Olifant for .NET](http://okapi.sourceforge.net/downloads.html).